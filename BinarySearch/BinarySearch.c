#include "BinarySearch.h"

int main (int argc, char* argv[]) {
	srand(time(NULL));
	int input_size = pow(2,4);
	int inputs = 7;
	int input_max = input_size*pow(2,(inputs-1));
	int input_number = 1;
	int* A = (int*)malloc(input_max*sizeof(int));
	for (int i = 0;  i < input_max; i++) {
		A[i] = i+1;
	}
	clock_t start, end;
	double time;
	double cumulative = 0;
	int cumulative_step = 1;
	printf("Binary Search, T(n)=O(log_2(n))\n");
	printf("n,t(n),T(n),t(n)*1000/T(n)\n");
	while (1) {
		start = clock();
		binary_search(A, 0, input_size-1, -1);
		end = clock();
		time = ((double)(end - start)) / CLOCKS_PER_SEC;
		cumulative += time;
		if (cumulative_step == 3) {
			double log2_input = log2(input_size);
			printf("%ld,%.4f,%.1f,%.2f\n", input_size, cumulative/3, log2_input, (cumulative/3*1000)/log2_input);
			cumulative_step = 0;
			cumulative = 0;
			input_number++;
			input_size = 2*input_size;
		}
		cumulative_step++;
		if (input_number > inputs) break;
	}
	printf("\n");
	free(A);
}

int binary_search(int* A, int l, int r, int t) {
	constant_time();
	if (l > r) return -1;
	int m = (l + r)/2;
	if (A[m] == t) return m;
	if (A[m] < t) return binary_search(A, m+1, r, t);
	else return binary_search(A, l, m-1, t);
}

// *Note: Binary Search needed a constant time function since it ran too fast even when using inputs of size 2^20.
void constant_time() {
	for (int i = 0; i < 1000000; i++) {}
}
