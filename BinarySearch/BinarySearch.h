#ifndef BINARYSEARCH_H
#define BINARYSEARCH_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int binary_search(int* A, int l, int r, int t);
void constant_time();

#endif
