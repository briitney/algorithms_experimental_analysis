#include "InsertionSort.h"

int main (int argc, char* argv[]) {
	srand(time(NULL));
	int input_size = pow(2,10);
	int inputs = 7;
	int input_max = input_size*pow(2,(inputs-1));
	int input_number = 1;
	int* A = (int*)malloc(input_max*sizeof(int));
	for (int i = 0;  i < input_max; i++) {
		A[i] = rand()%input_max;
	}
	clock_t start, end;
	double time;
	printf("Insertion Sort, T(n)=O(n^2)\n");
	printf("n,t(n),T(n),t(n)*10^10/T(n)\n");
	while (1) {
		start = clock();
		insertion_sort(A, input_size);
		end = clock();
		time = ((double)(end - start)) / CLOCKS_PER_SEC;
			double n_pow_2_input = pow(input_size,2);
			printf("%ld,%.4f,%.1f,%.2f\n", input_size, time, n_pow_2_input, (10000000000*time)/n_pow_2_input);
			input_number++;
			input_size = 2*input_size;
		if (input_number > inputs) break;
	}
	printf("\n");
	free(A);
}

void insertion_sort(int* input, int input_size) {
	int i, j, key;
	for (i = 1; i < input_size; i++) {
		key = input[i];
		j = i-1;
		while (j >= 0 && input[j] > key) {
			input[j+1] = input[j];
			j = j-1;
		}
		input[j+1] = key;
	}
}
