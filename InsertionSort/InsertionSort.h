#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void insertion_sort(int* input, int input_size);

#endif
