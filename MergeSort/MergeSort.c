#include "MergeSort.h"

int main (int argc, char* argv[]) {
	srand(time(NULL));
	int input_size = pow(2,14);
	int inputs = 7;
	int input_max = input_size*pow(2,(inputs-1));
	int input_number = 1;
	int* A = (int*)malloc(input_max*sizeof(int));
	int* B = (int*)malloc(input_max*sizeof(int));
	int* Master = (int*)malloc(input_max*sizeof(int));
	for (int i = 0;  i < input_max; i++) {
		Master[i] = rand()%input_size;
	}
	clock_t start, end;
	double time;
	printf("Merge Sort, T(n)=O(n*log_2(n))\n");
	printf("n,t(n),T(n),t(n)*10^6/T(n)\n");
	while (1) {
		for (int i = 0;  i < input_size; i++) {
			A[i] = Master[i];
			B[i] = Master[i];
		}
		start = clock();
		merge_sort(B, A, 0, input_size-1);
		end = clock();
		time = ((double)(end - start)) / CLOCKS_PER_SEC;
		double n_log2_n = input_size*log2(input_size);
		printf("%ld,%.4f,%.1f,%.2f\n", input_size, time, n_log2_n, (1000000*time/3)/n_log2_n);
		input_number++;
		input_size = 2*input_size;
		if (input_number > inputs) break;
	}
	printf("\n");
	free(A);
}

void merge(int* A, int* B, int p, int q, int r) {
	int i, j, k;
	i = p;
	j = r+1;
	k = p;
	while (1) {
		if (j > q) {
			B[k] = A[i];
			i++;
		} else if (i > r) {
			B[k] = A[j];
			j++;
		} else if (A[i] < A[j]) {
			B[k] = A[i];
			i++;
		} else {
			B[k] = A[j];
			j++;
		}
		k++;
		if (i > r && j > q) {
			break;
		}
	}
}

void merge_sort(int* A, int* B, int p, int q) {
	int r;
	waste_time();
	if (p < q) {
		r = (p+q)/2;
		merge_sort(B, A, p, r);
		merge_sort(B, A, r+1, q);
		merge(A, B, p, q, r);
	}
}

void waste_time() {
	for (int i = 0; i < 10000; i++) {}
}
