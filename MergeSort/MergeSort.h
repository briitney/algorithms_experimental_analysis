#ifndef MERGESORT_H
#define MERGESORT_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void merge(int* A, int* B, int p, int q, int r);
void merge_sort(int* A, int* B, int p, int q);
void waste_time();

#endif
