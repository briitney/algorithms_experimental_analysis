all:
	rm -f unformatted_results.txt
	make InsertionSort
	make MergeSort
	make BinarySearch

InsertionSort: InsertionSort/InsertionSort.c InsertionSort/InsertionSort.h
	g++ InsertionSort/InsertionSort.c -o test
	./test
	rm test

MergeSort: MergeSort/MergeSort.c MergeSort/MergeSort.h
	g++ MergeSort/MergeSort.c -o test
	./test
	rm test

BinarySearch: BinarySearch/BinarySearch.c BinarySearch/BinarySearch.h
	g++ BinarySearch/BinarySearch.c -o test
	./test
	rm test

clean:
	touch InsertionSort/InsertionSort.c
	touch MergeSort/MergeSort.c
	touch BinarySearch/BinarySearch.c
	make
